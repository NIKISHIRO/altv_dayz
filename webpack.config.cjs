const path  = require('path');
const webpack = require('webpack');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = (env) => {
  const ENTRY_CEF_FILE = path.join(__dirname, './frontend/index.tsx');
  const OUTPUT_CEF_DIR = path.join(__dirname, './src/core/client/webview/html');

  console.log('OUTPUT_CEF_DIR', OUTPUT_CEF_DIR);
  console.log('ENTRY_CEF_FILE', ENTRY_CEF_FILE);

  return {
    performance: {
      hints: false
    },  
    node:{
      fs: 'empty',
    },
    target: 'web',
    entry: ENTRY_CEF_FILE,
    output: {
      path: OUTPUT_CEF_DIR,
      filename: "index.js",
    },
    devServer: {
      contentBase: OUTPUT_CEF_DIR,
    },
    resolve: {
      extensions: ['.js', '.ts', '.tsx'],
      plugins: [new TsconfigPathsPlugin({
        configFile: './frontend/tsconfig.json'
      })],
    },
    plugins: [
    ],
    module: {
      rules: [
        { test: /\.tsx?$/, loaders: ['babel-loader', 'ts-loader'] },
        { test: /\.css$/, use: ["style-loader", "css-loader"] },
        { test: /\.(png|jpg|svg|gif|jpeg)$/, use: ['file-loader'] },
        { test: /\.(ttf|woff|woff2|eot)$/, use: ['file-loader'] },
        { test: /\.scss$/, loaders: ["style-loader","css-loader", "sass-loader"] },
      ],
    }
  };
}
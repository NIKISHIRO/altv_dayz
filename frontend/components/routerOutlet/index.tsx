import * as React from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import { HomeContainer } from '@pages/home/home.container';


export const RouterOutlet = (): JSX.Element => {
  return (
    <>
      <div style={{position: 'absolute'}}>
        <Link to={'/'}>Home</Link>
      </div>

      <Switch>
        <Route exact path={'/'} component={HomeContainer} />
        <Route exact path={'/test'} render={() => <div>dadaddadaadadad</div>} />
      </Switch>
    </>
  );
}

import * as React from 'react';
import { RouterOutlet } from '../routerOutlet';
import '../../global.css';


export const App = (): JSX.Element => {

  return (
    <div>
      <RouterOutlet />
    </div>
  );
}

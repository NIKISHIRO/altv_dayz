import React from "react";
import { Provider } from "react-redux";
import { store, memoryHistory } from "./redux/store";
import { App } from "./components/app";
import { ConnectedRouter } from "connected-react-router";
import { DndProvider } from "react-dnd";
import MouseBackEnd from 'react-dnd-mouse-backend';


const Root = (): JSX.Element => (
  <Provider store={store}>
    <ConnectedRouter history={memoryHistory}>
      <DndProvider backend={MouseBackEnd}>
        <App />
      </DndProvider>
    </ConnectedRouter>
  </Provider>
);

export {
  Root,
}

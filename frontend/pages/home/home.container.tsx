import React from "react";
import { handleEnvironment } from "@helpers";
import { Home } from "./home";


export const HomeContainer = () => {
  const [name, setName] = React.useState('');

  React.useEffect(() => {
    handleEnvironment(handleAltWebView, handleBrowser);
  }, []);

  const handleAltWebView = (alt: Alt) => {
    alt.on('webview:setname', (value: string) => {
      setName(value);
    });

    setName('NO_NAME');
  };

  const handleBrowser = () => {
    setName('NO_NAME');
  };

  return (
    <Home name={name} />
  );
};

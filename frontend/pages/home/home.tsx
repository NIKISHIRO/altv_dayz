import React from "react";


interface IProps {
  name: string;
}

export const Home: React.FC<IProps> = (props) => {

  return (
    <div>
      NAME IN HOME COMPONENT = {props.name}
    </div>
  );
};


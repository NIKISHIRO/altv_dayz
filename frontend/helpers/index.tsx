type THandleAltWebView = (alt: Alt) => void;


export const handleEnvironment = <T extends Function>(handleAltWebView: THandleAltWebView, handleBrowser: T) => {
  if ('alt' in window) return handleAltWebView(window.alt);

  handleBrowser();
};

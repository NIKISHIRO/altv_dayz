import { combineReducers, Reducer } from 'redux';
import { connectRouter, RouterState } from 'connected-react-router';


export type TRootState = {
  router: Reducer<RouterState<any>, any>,
};

const createRootReducer = (history: any): Reducer => combineReducers({
  router: connectRouter(history),
});

export {
  createRootReducer, 
};
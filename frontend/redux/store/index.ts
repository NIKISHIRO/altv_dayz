import { createStore, compose, applyMiddleware } from "redux";
import { createRootReducer } from "../reducers";
import { routerMiddleware } from "connected-react-router";
import { createMemoryHistory } from "history";
import thunk from 'redux-thunk';
import logger from 'redux-logger';


const history = createMemoryHistory();

const store = createStore(
  createRootReducer(history), 
  compose(
    applyMiddleware(
      routerMiddleware(history),
      thunk, 
      logger,
    ),
  ),
);

export {
  store,
  history as memoryHistory,
};
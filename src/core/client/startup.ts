import * as alt from 'alt-client';
import * as natives from 'natives';
import * as chat from './chat';


alt.onServer('log:Console', handleLogConsole);

function handleLogConsole(msg: string) {
  alt.log(msg);
}

const importModules = async () => {
  import('./chat/startup');
  import('./webview/startup');
};

importModules();



import * as alt from 'alt-client';


const webview = new alt.WebView('http://resource/client/webview/html/index.html');

const importModules = async () => {
  import('./commands');
};

webview.on('load', importModules);

export default webview;

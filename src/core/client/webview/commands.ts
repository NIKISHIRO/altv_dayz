import * as alt from 'alt-client';
import webview from '../webview/startup';


alt.onServer('client:setname', (value: string) => {
  webview.emit('webview:setname', value);
});

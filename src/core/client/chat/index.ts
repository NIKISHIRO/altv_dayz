import * as alt from "alt-client";

interface IBuffer {name: string, text: string}

const buffer: IBuffer[] = [];

const state = {
  opened: false,
  loaded: false,
}

const view = new alt.WebView("http://resource/client/chat/html/index.html");

console.log('importModules (2)');

function addMessage(name: string, text: string) {
  if (name) {
    view.emit("addMessage", name, text);
  } else {
    view.emit("addString", text);
  }
}

view.on("chatloaded", () => {
  for (const msg of buffer) {
    addMessage(msg.name, msg.text);
  }

  state.loaded = true;
});

view.on("chatmessage", (text: string) => {
  alt.emitServer("chat:message", text);

  state.opened = false;
  alt.toggleGameControls(true);
  view.unfocus();
});

export function pushMessage(name: string, text: string) {
  if (!state.loaded) {
    buffer.push({ name, text });
  } else {
    addMessage(name, text);
  }
}

export function pushLine(text: string) {
  pushMessage(null, text);
}

alt.onServer("chat:message", pushMessage);

alt.on("keyup", (key) => {
  if (state.loaded) {
    if (!state.opened && key === 0x54 && alt.gameControlsEnabled()) {
      state.opened = true;
      view.emit("openChat", false);
      alt.toggleGameControls(false);
      view.focus();
    } else if (!state.opened && key === 0xbf && alt.gameControlsEnabled()) {
      state.opened = true;
      view.emit("openChat", true);
      alt.toggleGameControls(false);
      view.focus();
    } else if (state.opened && key == 0x1b) {
      state.opened = false;
      view.emit("closeChat");
      alt.toggleGameControls(true);
      view.unfocus();
    }
  }
});

pushLine("<b>alt:V Multiplayer has started</b>");

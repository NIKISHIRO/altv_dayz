import * as alt from 'alt-client';
import * as game from 'natives';


const player = alt.Player.local;

alt.onServer('client:createVehicle', (vehicleScriptId: number) => {
  console.log('vehicleScriptId', vehicleScriptId);
  console.log('typeof vehicleScriptId', typeof vehicleScriptId);

  // native.setPedIntoVehicle()
});

alt.onServer('client:createObject', (object) => {
  console.log('client:createObject');

  requestModelPromise(game.getHashKey(object)).then((succ) => {
    if (succ) {
      const newObjectToPlace = game.createObject(
        game.getHashKey(object),
        player.pos.x,
        player.pos.y,
        player.pos.z,
        true,
        false,
        true,
      );
      game.setEntityAlpha(newObjectToPlace, 150, true);
      game.setEntityCollision(newObjectToPlace, true, true);

      console.log('newObjectToPlace', newObjectToPlace);
    }
  });
});

function requestModelPromise(model) {
  return new Promise((resolve, reject) => {
    if (!game.hasModelLoaded(model)) {
      game.requestModel(model);
    }

    // return resolve(false);
    let inter = alt.setInterval(() => {
      if (game.hasModelLoaded(model)) {
        alt.clearInterval(inter);
        return resolve(true);
      }
    }, 10);
  });
}

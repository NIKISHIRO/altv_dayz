import * as alt from 'alt-server';

const cmdHandlers: { [command: string]: Function } = {};
const mutedPlayers = new Map<alt.Player, number>();

function invokeCmd(player: alt.Player, cmd: string, args) {
  cmd = cmd.toLowerCase();
  const callback = cmdHandlers[cmd];

  if (callback) {
    callback(player, args);
  } else {
    send(player, `{FF0000} Unknown command /${cmd}`);
  }
}

alt.onClient('chat:message', (player: alt.Player, msg: string) => {
  if (msg[0] === '/') {
    msg = msg.trim().slice(1);

    if (msg.length > 0) {
      alt.log('[chat:cmd] ' + player.name + ': /' + msg);

      const args = msg.split(' ');
      const cmd = args.shift();

      invokeCmd(player, cmd, args);
    }
  } else {
    if (mutedPlayers.has(player) && mutedPlayers.get(player)) {
      send(player, '{FF0000} You are currently muted.');
      return;
    }

    msg = msg.trim();

    if (msg.length > 0) {
      alt.log('[chat:msg] ' + player.name + ': ' + msg);

      alt.emitClient(
        null,
        'chat:message',
        player.name,
        msg.replace(/</g, '&lt;').replace(/'/g, '&#39').replace(/"/g, '&#34')
      );
    }
  }
});

export function send(player: alt.Player, msg: string) {
  alt.emitClient(player, 'chat:message', null, msg);
}

export function broadcast(msg: string) {
  send(null, msg);
}

export function registerCmd(cmd: string, callback: (player: alt.Player, args: string[]) => void) {
  cmd = cmd.toLowerCase();

  if (cmdHandlers[cmd] !== undefined) {
    alt.logError(`Failed to register command /${cmd}, already registered`);
  } else {
    cmdHandlers[cmd] = callback;
  }
}

export function mutePlayer(player: alt.Player, state: number) {
  mutedPlayers.set(player, state);
}

// Used in an onConnect function to add functions to the player entity for a seperate resource.
export function setupPlayer(player: alt.Player) {
  player.sendMessage = (msg: string) => {
    send(player, msg);
  };

  player.mute = (state: number) => {
    mutePlayer(player, state);
  };
}

// Arbitrary events to call.
alt.on('sendChatMessage', (player: alt.Player, msg: string) => {
  alt.logWarning('Usage of chat events is deprecated use export functions instead');
  send(player, msg);
});

alt.on('broadcastMessage', (msg: string) => {
  alt.logWarning('Usage of chat events is deprecated use export functions instead');
  send(null, msg);
});


import * as alt from 'alt-server';
import * as chat from '../chat';


chat.registerCmd('name', (player: alt.Player, args: string) => {
  const value = args[0];

  if (!args || typeof value !== 'string') {
    return chat.send(player, '/name [model]');
  }

  alt.emitClient(player, 'client:setname', value);
});

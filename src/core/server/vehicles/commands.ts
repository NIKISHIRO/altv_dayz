import * as alt from 'alt-server';
import * as chat from '../chat';


chat.registerCmd('veh', (player, args) => {
  const model = args[0];

  if (!args || typeof args[0] !== 'string') {
    return chat.send(player, '/veh [model]');
  }

  try {
    const { x, y, z } = player.pos;
    const vehicle = new alt.Vehicle(model, x, y, z, 0, 0, 0);

    alt.emitClient(player, 'client:createVehicle', vehicle);
  }
  catch (err) {
    chat.send(player, 'Не корректное название модели');
  }
});

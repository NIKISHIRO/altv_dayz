import * as alt from 'alt-server';
import * as chat from '../chat';


chat.registerCmd('obj', (player, args) => {
  if (!args || typeof args[0] !== 'string') {
    return chat.send(player, '/obj [object]');
  }

  alt.emitClient(player, 'client:createObject', args[0]);
});

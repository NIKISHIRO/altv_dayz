declare module 'alt-server' {
  interface Player {
    sendMessage: (msg: string) => void;
    mute: (state: number) => void;
  }
}
